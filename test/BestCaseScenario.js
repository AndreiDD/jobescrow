

const JobEscrow = artifacts.require("./JobEscrow.sol");


contract('JobEscrowsol Javascript Tests', async (accounts) => {

  let instance

  beforeEach('setup a new contract for each test', async function () {
    instance = await JobEscrow.deployed()
    // 1234 is a tokenID from JobGenerator
    await instance.createEscrow(1234, { from: accounts[0], value: 2e18 })
  })


  it("a best case scenario", async () => {
    const company_balance_initial = await web3.eth.getBalance(accounts[1]);

    let value = parseFloat(await instance.value());
    expect(value, "Initial Escrowed Value is 1 ETH").to.equal(1e18);

    var receipt = await instance.confirmFreelancer({ from: accounts[1], value: 2e18 })
    var gasUsed = receipt.receipt.gasUsed;
    var tx = await web3.eth.getTransaction(receipt.tx);

    let confirmFreelancerGasCost = tx.gasPrice * gasUsed

    var company_balance_after = await web3.eth.getBalance(accounts[1]);
    expect(parseFloat(company_balance_after), "After company has 98 ETH").to.equal(company_balance_initial - 2e18 - confirmFreelancerGasCost);

    receipt = await instance.confirmJobDone({ from: accounts[1] })
    gasUsed = receipt.receipt.gasUsed;
    tx = await web3.eth.getTransaction(receipt.tx);

    let confirmJobDoneGasCost = tx.gasPrice * gasUsed

    var company_balance_final = await web3.eth.getBalance(accounts[1]);
    expect(parseFloat(company_balance_final), "After company lost 1 ETH").to.equal(company_balance_initial - 1e18 - confirmJobDoneGasCost - confirmFreelancerGasCost);

  })


});
