

const JobEscrow = artifacts.require("./JobEscrow.sol");


contract('JobEscrowsol Javascript Tests', async (accounts) => {

  let instance

  beforeEach('setup a new contract for each test', async function () {
    instance = await JobEscrow.deployed()
    // 1234 is a tokenID from JobGenerator
    await instance.createEscrow(1234, { from: accounts[0], value: 2e18 })
  })

  it("freelancers account is account 0", async () => {
    let value = (await instance.freelancer()).toString();
    expect(value, "Freelancer's account == accounts[0]").to.equal(accounts[0]);
  })

  it("initial value is 1 ETH", async () => {
    let value = parseFloat(await instance.value());
    expect(value, "Initial Escrowed Value is 1 ETH").to.equal(1e18);
  })

  it("when a company agrees, it has to escrow 2 ETH", async () => {
    const account_1_balance_initial = await web3.eth.getBalance(accounts[1]);
    expect(parseFloat(account_1_balance_initial), "Initial company has 100 ETH").to.equal(100e18);

    let value = parseFloat(await instance.value());
    expect(value, "Initial Escrowed Value is 1 ETH").to.equal(1e18);

    const receipt = await instance.confirmFreelancer({ from: accounts[1], value: 2e18 })
    const gasUsed = receipt.receipt.gasUsed;
    const tx = await web3.eth.getTransaction(receipt.tx);

    const account_1_balance_after = await web3.eth.getBalance(accounts[1]);
    expect(parseFloat(account_1_balance_after), "After company has 98 ETH").to.equal(98e18 - tx.gasPrice * gasUsed);

    // contract state is now 1
    let state = parseFloat(await instance.state());
    expect(state, "State is now set to 1").to.equal(1);

  })

});
