package database

import (
	log "github.com/sirupsen/logrus"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// Db is the database reference
var Db *sqlx.DB

const connectionURL = "user:password@tcp(serverIP:3306)/dbName"

// InitMySQLDatabase inits the database
func InitMySQLDatabase() {
	var err error
	if Db != nil {
		err = Db.Ping()
		if err == nil {
			return
		} else {
			log.Errorf(err.Error())
		}
	}

	Db, err = sqlx.Open("mysql", connectionURL)
	if err != nil {
		log.Fatalln("open mysql failed,", err)
		return
	}
	log.Println("connected ok to the " + Db.DriverName() + " database ")

}
