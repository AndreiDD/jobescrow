package database

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"jobescrow/models"
	"jobescrow/other"
	"time"
)

// CreateCompany adds a new company into the database
func CreateCompany(company models.Company) error {

	newID := uuid.NewV4().String()[0:8]
	if company.ID != "" {
		newID = company.ID
	}

	newToken := other.SHA256(uuid.NewV4().String())
	if company.Token != "" {
		newToken = company.Token
	}

	_, err := Db.NamedExec(`INSERT INTO companies (id, email, hashed_password, company_name, company_logo, last_name, first_name, billing_contact_first_name, billing_contact_last_name, billing_address, billing_phone, billing_email, alternate_billing_email, token, date_created, account_balance, vat_number)
	     VALUES (:id, :email, :hashed_password, :company_name, :company_logo, :last_name, :first_name, :billing_contact_first_name, :billing_contact_last_name, :billing_address, :billing_phone,
	             :billing_email, :alternate_billing_email, :token, :date_created, :account_balance, :vat_number)`,
		map[string]interface{}{
			"id":                         newID,
			"token":                      newToken,
			"hashed_password":            company.HashedPassword,
			"email":                      company.Email,
			"last_name":                  company.LastName,
			"first_name":                 company.FirstName,
			"company_name":               company.CompanyName,
			"company_logo":               company.CompanyLogo,
			"billing_contact_first_name": company.BillingContactFirstName,
			"billing_contact_last_name":  company.BillingContactLastName,
			"billing_address":            company.BillingAddress,
			"billing_phone":              company.BillingPhone,
			"billing_email":              company.BillingEmail,
			"alternate_billing_email":    company.AlternateBillingEmail,
			"date_created":               time.Now().Unix(),
			"account_balance":            company.AccountBalance,
			"vat_number":                 company.VatNumber,
		})

	if err != nil {
		log.Error(err)
		return fmt.Errorf("invalid request. please contact support")
	}

	return nil
}

func DeleteCompany(id string) error {
	query := `DELETE FROM companies WHERE id = ?`
	res, err := Db.Exec(query, id)
	if err != nil {
		log.Error(err)
		return fmt.Errorf("delete company failed %s", err.Error())
	}
	log.Info(res)
	return nil
}

// GetCompanyIDByToken returns an employeeID by it's token
func GetCompanyIDByToken(token string) (string, error) {

	var id string
	err := Db.Get(&id, "SELECT id FROM companies WHERE token=?", token)
	if err != nil {
		return "", fmt.Errorf("can't query for the company by token %s", err.Error())
	}

	return id, nil
}

// GetCompanyByID returns a company by it's id
func GetCompanyByID(id string) (models.Company, error) {

	var company models.Company
	err := Db.Get(&company, "SELECT * FROM companies WHERE id=?", id)
	if err != nil {
		return company, fmt.Errorf("can't query for the company with id %s -> %s", id, err.Error())
	}
	return company, nil
}

// GetAllCompaniesDB
func GetAllCompaniesDB(offset int, limit int) ([]models.Company, error) {

	var companies []models.Company

	if limit > 1000 {
		return companies, fmt.Errorf("limit is 1000 records per query")
	}
	if offset < 0 {
		return companies, fmt.Errorf("invalid offset")
	}

	err := Db.Select(&companies, "SELECT * FROM companies LIMIT ?,?", offset, limit)
	if err != nil {
		return companies, fmt.Errorf("can't query for companies %s", err.Error())
	}

	return companies, nil
}
