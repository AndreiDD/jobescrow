package database

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	"jobescrow/models"
	"jobescrow/other"
	"time"
)

// CreateJob mints a new job
func CreateJob(job models.Job) error {

	// assign a new id if it doesn't already have
	newID := uuid.NewV4().String()[0:8]
	if job.ID != "" {
		newID = job.ID
	}

	return nil
	}

// GetAllJobsDB retrives all jobs from the database (paginated)
func GetAllJobsDB(offset int, limit int) ([]models.Job, error) {

	var jobs []models.Job

	if limit > 1000 {
		return jobs, fmt.Errorf("limit is 1000 records per query")
	}
	if offset < 0 {
		return jobs, fmt.Errorf("invalid offset")
	}

	err := Db.Select(&jobs, "SELECT * FROM jobs LIMIT ?,?", offset, limit)
	if err != nil {
		return jobs, fmt.Errorf("can't query for jobs %s", err.Error())
	}

	return jobs, nil
}
