package database

import (
	"github.com/stretchr/testify/assert"
	"jobescrow/models"
	"testing"
)

func init() {
	InitMySQLDatabase()
}

func TestCreateCompany(t *testing.T) {
	err := CreateCompany(models.Company{ID: "testID", FirstName: "Testus1", LastName: "Companyus2"})
	if err != nil {
		t.Errorf("TestCreateCompany() error = %v", err)
	}

	err = DeleteCompany("testID")
	if err != nil {
		t.Errorf("DeleteCompany() error = %v", err)
	}
}

func TestGetCompanyIDByToken(t *testing.T) {
	employeeID, err := GetCompanyIDByToken("abc-def-xyz")
	if err != nil {
		t.Errorf("employeeID() error = %v", err)
	}
	t.Logf("got %s CompanyID\n", employeeID)

	assert.True(t, len(employeeID) > 0)
}

func TestGetAllCompanies(t *testing.T) {

	companies, err := GetAllCompaniesDB(0, 10)
	if err != nil {
		t.Errorf("GetAllCompaniesDB() error = %v", err)
	}
	t.Logf("got %d companies\n", len(companies))

	assert.True(t, len(companies) > 0)

}

func TestGetCompanyIDByID(t *testing.T) {
	Company, err := GetCompanyByID("1a2b4c")
	if err != nil {
		t.Errorf("GetCompanyByID() error = %v", err)
	}
	t.Logf("got %s Company\n", Company.Email)
}
