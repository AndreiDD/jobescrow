package main

import "jobescrow/handlers"

// InitializeRoutes initializes all the routes in the app
func InitializeRoutes() {

	v1 := router.Group("/api/")
	{
		// gets all companies
		v1.GET("/companies", handlers.GetAllCompanies)

		// creates a new company
		v1.POST("/company", handlers.AddCompany)

	}
}
