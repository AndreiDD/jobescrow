package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"squsers/database"
	"squsers/models"
)

func main() {
	fmt.Println("Starting filling up the database with companies...")

	database.InitMySQLDatabase()

	pwd, _ := os.Getwd()
	jsonFile, err := os.Open(pwd + "/mock_companies.json")
	if err != nil {
		log.Fatalf("cant find the file %s", err.Error())
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var companies []models.Company
	err = json.Unmarshal(byteValue, &companies)
	if err != nil {
		log.Fatalf("error unmarshaling %s", err.Error())
	}

	// for testing
	var aCompany models.Company
	aCompany.ID = "1a2b4c"
	aCompany.Token = "abc-def-xyz"
	aCompany.FirstName = "Andreas"
	aCompany.LastName = "Danieli"
	aCompany.CompanyName = "AVID OOD"
	aCompany.Email = "support@avid.bg"
	aCompany.BillingPhone = "210312031023"
	_ = database.CreateCompany(aCompany)

	for _, company := range companies {
		_ = database.CreateCompany(company)
	}

	fmt.Println("------------ done.")

}
