package other

import (
	"crypto/sha256"
	"encoding/hex"
)

// SHA256 hashes using SHA256 algorithm
func SHA256(text string) string {
	algorithm := sha256.New()
	algorithm.Write([]byte(text))
	return hex.EncodeToString(algorithm.Sum(nil))
}
