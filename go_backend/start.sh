#!/usr/bin/env bash
pm2 stop "job_escrow"
git pull
go build -o job_escrow
pm2 start "job_escrow"
pm2 logs "job_escrow"
