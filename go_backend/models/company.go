package models

type Company struct {
	ID                      string `db:"id"  json:"id"`
	Token                   string `db:"token"  json:"token"`
	Email                   string `db:"email"  json:"email"  binding:"required"`
	HashedPassword          string `db:"hashed_password"  json:"hashed_password"`
	CompanyName             string `db:"company_name"  json:"company_name"`
	CompanyLogo             string `db:"company_logo"  json:"company_logo"`
	LastName                string `db:"last_name"  json:"last_name"`
	FirstName               string `db:"first_name"  json:"first_name"`
	BillingContactFirstName string `db:"billing_contact_first_name"  json:"billing_contact_first_name"`
	BillingContactLastName  string `db:"billing_contact_last_name"  json:"billing_contact_last_name"`
	BillingAddress          string `db:"billing_address"  json:"billing_address"`
	BillingPhone            string `db:"billing_phone"  json:"billing_phone"`
	BillingEmail            string `db:"billing_email"  json:"billing_email"`
	AlternateBillingEmail   string `db:"alternate_billing_email"  json:"alternate_billing_email"`
	DateCreated             int64  `db:"date_created"  json:"date_created"`
	AccountBalance          int64  `db:"account_balance"  json:"account_balance"`
	VatNumber               string `db:"vat_number"  json:"vat_number"`
}
