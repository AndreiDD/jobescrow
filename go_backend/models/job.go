package models

type Job struct {
	ID                      string `db:"id"  json:"id"`
	Title                   string `db:"title"  json:"title" binding:"required"`
	Description             string `db:"description"  json:"description"  binding:"required"`
	Amount                  float64 `db:"amount"  json:"amount" binding:"required"`
	Currency                string `db:"currency"  json:"currency" binding:"required"`
	LimitStartDate          int64 `db:"limit_start_date"  json:"limit_start_date"`
	ExpirationDate          int64 `db:"expiration_date"  json:"expiration_date"`
	CompanyID               string `db:"company_id"  json:"company_id" binding:"required"`
	DateCreated             int64  `db:"date_created"  json:"date_created"`
}
