package handlers

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"net/http"
	"jobescrow/database"
	"jobescrow/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

// AddCompany creates a new company
func AddCompany(c *gin.Context) {

	var company models.Company

	err := c.BindJSON(&company)
	if err != nil {
		log.Error(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid payload " + err.Error()})
		return
	}

	err = newCompanyValidation(company)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	err = database.CreateCompany(company)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.Status(http.StatusOK)
}

// validates the form
func newCompanyValidation(company models.Company) error {
	if company.FirstName == "" {
		return fmt.Errorf("please complete first name")
	}
	return nil
}

// GetAllCompanies ...
func GetAllCompanies(c *gin.Context) {
	offset := c.DefaultQuery("offset", "0")
	limit := c.DefaultQuery("limit", "100")
	intOffset, err := strconv.Atoi(offset)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	intLimit, err := strconv.Atoi(limit)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	employees, err := database.GetAllCompaniesDB(intOffset, intLimit)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(200, employees)
}
