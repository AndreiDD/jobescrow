module jobescrow

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.2.2
	github.com/ugorji/go v1.1.4 // indirect
	github.com/unrolled/secure v1.0.0
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
