ETH Job Escrow System
==============================


Quick-start
-----------


Arhitecture intro:

- Since each job is unique, I choose a modified ERC721 token as the base arhitecture for each job.

-> job -> holds a metadata consisting of a base64 encoding of a json in the following format

{"job_url":"...", "text_hash":"sha256 of the text" }

The idea is to write as little as possible in the blockchain but having the hash of the text for the document so it can be verified.

The Escrow Contract is a standard escrow contract, that keep diffrent states of the job

An example of a go backend is under:

https://gitlab.com/AndreiDD/jobescrow/tree/master/go_backend


## How to build

Clone and install dependencies:

```
$ git clone ........
$ cd .....
$ npm install
```

Ensure you have `ganache-cli` installed and running:

```
$ npm install -g ganache-cli
$ ganache-cli
```

### Check the package.json for some usefull scripts

In a different terminal session, ensure the contracts are working as expected:

```
$ npm test
```

Deployment of the smart contracts:

You first need to create the config/secret.json with your credentials.

```
truffle migrate
```
