// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package smartcontracts

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// SmartcontractsABI is the input ABI used to generate the binding from.
const SmartcontractsABI = "[{\"constant\":false,\"inputs\":[],\"name\":\"confirmJobDone\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"abort\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"value\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"company\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"disputeAsFreelancer\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"_tokenID\",\"type\":\"uint256\"}],\"name\":\"createEscrow\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"freelancer\",\"outputs\":[{\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"tokenID\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"disputeAsCompany\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"state\",\"outputs\":[{\"name\":\"\",\"type\":\"uint8\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"confirmFreelancer\",\"outputs\":[],\"payable\":true,\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"Aborted\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"ConfirmedFreelancer\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"JobFinishedSuccessfully\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"DisputeCompany\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[],\"name\":\"DisputeFreelancer\",\"type\":\"event\"}]"

// SmartcontractsBin is the compiled bytecode used for deploying new contracts.
const SmartcontractsBin = `608060405234801561001057600080fd5b5033600260006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550610e4f806100616000396000f3fe6080604052600436106100b9576000357c0100000000000000000000000000000000000000000000000000000000900480638da2c873116100815780638da2c87314610185578063a37dda2c146101b3578063a5c42ef11461020a578063c152151814610235578063c19d93fb1461024c578063c7e13a2a14610285576100b9565b806318083adc146100be57806335a063b4146100d55780633fa4f245146100ec5780636904c94d146101175780638ab5c8741461016e575b600080fd5b3480156100ca57600080fd5b506100d361028f565b005b3480156100e157600080fd5b506100ea610513565b005b3480156100f857600080fd5b5061010161072c565b6040518082815260200191505060405180910390f35b34801561012357600080fd5b5061012c610732565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561017a57600080fd5b50610183610758565b005b6101b16004803603602081101561019b57600080fd5b81019080803590602001909291905050506108f0565b005b3480156101bf57600080fd5b506101c8610a14565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34801561021657600080fd5b5061021f610a3a565b6040518082815260200191505060405180910390f35b34801561024157600080fd5b5061024a610a40565b005b34801561025857600080fd5b50610261610bd8565b6040518082600381111561027157fe5b60ff16815260200191505060405180910390f35b61028d610beb565b005b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610337576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526028815260200180610dc76028913960400191505060405180910390fd5b600180600381111561034557fe5b600360149054906101000a900460ff16600381111561036057fe5b1415156103d5576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260138152602001807f696e636f7272656374206a6f622073746174650000000000000000000000000081525060200191505060405180910390fd5b6002600360146101000a81548160ff021916908360038111156103f457fe5b02179055507facfc25a9e10ece6a9b3ee7c66ca782e6c75cfcd6171fa0063471e3b038a1ce9460405160405180910390a1600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc6001549081150290604051600060405180830381858888f1935050505015801561048f573d6000803e3d6000fd5b50600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc3073ffffffffffffffffffffffffffffffffffffffff16319081150290604051600060405180830381858888f1935050505015801561050f573d6000803e3d6000fd5b5050565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156105bb576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602b815260200180610d9c602b913960400191505060405180910390fd5b60008060038111156105c957fe5b600360149054906101000a900460ff1660038111156105e457fe5b141515610659576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260138152602001807f696e636f7272656374206a6f622073746174650000000000000000000000000081525060200191505060405180910390fd5b7f72c874aeff0b183a56e2b79c71b46e1aed4dee5e09862134b8821ba2fddbf8bf60405160405180910390a16002600360146101000a81548160ff021916908360038111156106a457fe5b0217905550600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166108fc3073ffffffffffffffffffffffffffffffffffffffff16319081150290604051600060405180830381858888f19350505050158015610728573d6000803e3d6000fd5b5050565b60015481565b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610800576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602b815260200180610d9c602b913960400191505060405180910390fd5b600180600381111561080e57fe5b600360149054906101000a900460ff16600381111561082957fe5b14151561089e576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260138152602001807f696e636f7272656374206a6f622073746174650000000000000000000000000081525060200191505060405180910390fd5b60038060146101000a81548160ff021916908360038111156108bc57fe5b02179055507f42f622ee008bee30fa709b5e993e0b340494d2ece4b36bfac89bd82b65aadff760405160405180910390a150565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610998576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252602b815260200180610d9c602b913960400191505060405180910390fd5b6002348115156109a457fe5b0460018190555034600154600202141515610a0a576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526035815260200180610def6035913960400191505060405180910390fd5b8060008190555050565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b60005481565b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610ae8576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401808060200182810382526028815260200180610dc76028913960400191505060405180910390fd5b6001806003811115610af657fe5b600360149054906101000a900460ff166003811115610b1157fe5b141515610b86576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260138152602001807f696e636f7272656374206a6f622073746174650000000000000000000000000081525060200191505060405180910390fd5b60038060146101000a81548160ff02191690836003811115610ba457fe5b02179055507f13ab9f3771939d079cbbfd3fac6d5eb557018fb491aa068c5ecaae963ab3fdcc60405160405180910390a150565b600360149054906101000a900460ff1681565b6000806003811115610bf957fe5b600360149054906101000a900460ff166003811115610c1457fe5b141515610c89576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260138152602001807f696e636f7272656374206a6f622073746174650000000000000000000000000081525060200191505060405180910390fd5b6001546002023414801515610d06576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004018080602001828103825260118152602001807f696e76616c696420636f6e646974696f6e00000000000000000000000000000081525060200191505060405180910390fd5b7fcb099c16b5ac439595a04aee5d3d98570ffc37e017512ba4f9ed0fb6da09edc060405160405180910390a133600360006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506001600360146101000a81548160ff02191690836003811115610d9257fe5b0217905550505056fe6f6e6c792074686520667265656c616e63657220697320616c6c6f77656420746f2063616c6c20746869736f6e6c792074686520636f6d70616e7920697320616c6c6f77656420746f2063616c6c2074686973667265656c616e636572206d7573742073746172742069742077697468203278207468652076616c7565206f6620746865206a6f62a165627a7a72305820a50bed6b70e657e8610b9c20175991f2775bd6f25a96b946b1900e35983b722f0029`

// DeploySmartcontracts deploys a new Ethereum contract, binding an instance of Smartcontracts to it.
func DeploySmartcontracts(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *Smartcontracts, error) {
	parsed, err := abi.JSON(strings.NewReader(SmartcontractsABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(SmartcontractsBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Smartcontracts{SmartcontractsCaller: SmartcontractsCaller{contract: contract}, SmartcontractsTransactor: SmartcontractsTransactor{contract: contract}, SmartcontractsFilterer: SmartcontractsFilterer{contract: contract}}, nil
}

// Smartcontracts is an auto generated Go binding around an Ethereum contract.
type Smartcontracts struct {
	SmartcontractsCaller     // Read-only binding to the contract
	SmartcontractsTransactor // Write-only binding to the contract
	SmartcontractsFilterer   // Log filterer for contract events
}

// SmartcontractsCaller is an auto generated read-only Go binding around an Ethereum contract.
type SmartcontractsCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SmartcontractsTransactor is an auto generated write-only Go binding around an Ethereum contract.
type SmartcontractsTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SmartcontractsFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type SmartcontractsFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// SmartcontractsSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type SmartcontractsSession struct {
	Contract     *Smartcontracts   // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// SmartcontractsCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type SmartcontractsCallerSession struct {
	Contract *SmartcontractsCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts         // Call options to use throughout this session
}

// SmartcontractsTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type SmartcontractsTransactorSession struct {
	Contract     *SmartcontractsTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts         // Transaction auth options to use throughout this session
}

// SmartcontractsRaw is an auto generated low-level Go binding around an Ethereum contract.
type SmartcontractsRaw struct {
	Contract *Smartcontracts // Generic contract binding to access the raw methods on
}

// SmartcontractsCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type SmartcontractsCallerRaw struct {
	Contract *SmartcontractsCaller // Generic read-only contract binding to access the raw methods on
}

// SmartcontractsTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type SmartcontractsTransactorRaw struct {
	Contract *SmartcontractsTransactor // Generic write-only contract binding to access the raw methods on
}

// NewSmartcontracts creates a new instance of Smartcontracts, bound to a specific deployed contract.
func NewSmartcontracts(address common.Address, backend bind.ContractBackend) (*Smartcontracts, error) {
	contract, err := bindSmartcontracts(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Smartcontracts{SmartcontractsCaller: SmartcontractsCaller{contract: contract}, SmartcontractsTransactor: SmartcontractsTransactor{contract: contract}, SmartcontractsFilterer: SmartcontractsFilterer{contract: contract}}, nil
}

// NewSmartcontractsCaller creates a new read-only instance of Smartcontracts, bound to a specific deployed contract.
func NewSmartcontractsCaller(address common.Address, caller bind.ContractCaller) (*SmartcontractsCaller, error) {
	contract, err := bindSmartcontracts(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &SmartcontractsCaller{contract: contract}, nil
}

// NewSmartcontractsTransactor creates a new write-only instance of Smartcontracts, bound to a specific deployed contract.
func NewSmartcontractsTransactor(address common.Address, transactor bind.ContractTransactor) (*SmartcontractsTransactor, error) {
	contract, err := bindSmartcontracts(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &SmartcontractsTransactor{contract: contract}, nil
}

// NewSmartcontractsFilterer creates a new log filterer instance of Smartcontracts, bound to a specific deployed contract.
func NewSmartcontractsFilterer(address common.Address, filterer bind.ContractFilterer) (*SmartcontractsFilterer, error) {
	contract, err := bindSmartcontracts(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &SmartcontractsFilterer{contract: contract}, nil
}

// bindSmartcontracts binds a generic wrapper to an already deployed contract.
func bindSmartcontracts(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(SmartcontractsABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Smartcontracts *SmartcontractsRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Smartcontracts.Contract.SmartcontractsCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Smartcontracts *SmartcontractsRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.Contract.SmartcontractsTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Smartcontracts *SmartcontractsRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Smartcontracts.Contract.SmartcontractsTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Smartcontracts *SmartcontractsCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _Smartcontracts.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Smartcontracts *SmartcontractsTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Smartcontracts *SmartcontractsTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Smartcontracts.Contract.contract.Transact(opts, method, params...)
}

// Company is a free data retrieval call binding the contract method 0x6904c94d.
//
// Solidity: function company() constant returns(address)
func (_Smartcontracts *SmartcontractsCaller) Company(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _Smartcontracts.contract.Call(opts, out, "company")
	return *ret0, err
}

// Company is a free data retrieval call binding the contract method 0x6904c94d.
//
// Solidity: function company() constant returns(address)
func (_Smartcontracts *SmartcontractsSession) Company() (common.Address, error) {
	return _Smartcontracts.Contract.Company(&_Smartcontracts.CallOpts)
}

// Company is a free data retrieval call binding the contract method 0x6904c94d.
//
// Solidity: function company() constant returns(address)
func (_Smartcontracts *SmartcontractsCallerSession) Company() (common.Address, error) {
	return _Smartcontracts.Contract.Company(&_Smartcontracts.CallOpts)
}

// Freelancer is a free data retrieval call binding the contract method 0xa37dda2c.
//
// Solidity: function freelancer() constant returns(address)
func (_Smartcontracts *SmartcontractsCaller) Freelancer(opts *bind.CallOpts) (common.Address, error) {
	var (
		ret0 = new(common.Address)
	)
	out := ret0
	err := _Smartcontracts.contract.Call(opts, out, "freelancer")
	return *ret0, err
}

// Freelancer is a free data retrieval call binding the contract method 0xa37dda2c.
//
// Solidity: function freelancer() constant returns(address)
func (_Smartcontracts *SmartcontractsSession) Freelancer() (common.Address, error) {
	return _Smartcontracts.Contract.Freelancer(&_Smartcontracts.CallOpts)
}

// Freelancer is a free data retrieval call binding the contract method 0xa37dda2c.
//
// Solidity: function freelancer() constant returns(address)
func (_Smartcontracts *SmartcontractsCallerSession) Freelancer() (common.Address, error) {
	return _Smartcontracts.Contract.Freelancer(&_Smartcontracts.CallOpts)
}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() constant returns(uint8)
func (_Smartcontracts *SmartcontractsCaller) State(opts *bind.CallOpts) (uint8, error) {
	var (
		ret0 = new(uint8)
	)
	out := ret0
	err := _Smartcontracts.contract.Call(opts, out, "state")
	return *ret0, err
}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() constant returns(uint8)
func (_Smartcontracts *SmartcontractsSession) State() (uint8, error) {
	return _Smartcontracts.Contract.State(&_Smartcontracts.CallOpts)
}

// State is a free data retrieval call binding the contract method 0xc19d93fb.
//
// Solidity: function state() constant returns(uint8)
func (_Smartcontracts *SmartcontractsCallerSession) State() (uint8, error) {
	return _Smartcontracts.Contract.State(&_Smartcontracts.CallOpts)
}

// TokenID is a free data retrieval call binding the contract method 0xa5c42ef1.
//
// Solidity: function tokenID() constant returns(uint256)
func (_Smartcontracts *SmartcontractsCaller) TokenID(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _Smartcontracts.contract.Call(opts, out, "tokenID")
	return *ret0, err
}

// TokenID is a free data retrieval call binding the contract method 0xa5c42ef1.
//
// Solidity: function tokenID() constant returns(uint256)
func (_Smartcontracts *SmartcontractsSession) TokenID() (*big.Int, error) {
	return _Smartcontracts.Contract.TokenID(&_Smartcontracts.CallOpts)
}

// TokenID is a free data retrieval call binding the contract method 0xa5c42ef1.
//
// Solidity: function tokenID() constant returns(uint256)
func (_Smartcontracts *SmartcontractsCallerSession) TokenID() (*big.Int, error) {
	return _Smartcontracts.Contract.TokenID(&_Smartcontracts.CallOpts)
}

// Value is a free data retrieval call binding the contract method 0x3fa4f245.
//
// Solidity: function value() constant returns(uint256)
func (_Smartcontracts *SmartcontractsCaller) Value(opts *bind.CallOpts) (*big.Int, error) {
	var (
		ret0 = new(*big.Int)
	)
	out := ret0
	err := _Smartcontracts.contract.Call(opts, out, "value")
	return *ret0, err
}

// Value is a free data retrieval call binding the contract method 0x3fa4f245.
//
// Solidity: function value() constant returns(uint256)
func (_Smartcontracts *SmartcontractsSession) Value() (*big.Int, error) {
	return _Smartcontracts.Contract.Value(&_Smartcontracts.CallOpts)
}

// Value is a free data retrieval call binding the contract method 0x3fa4f245.
//
// Solidity: function value() constant returns(uint256)
func (_Smartcontracts *SmartcontractsCallerSession) Value() (*big.Int, error) {
	return _Smartcontracts.Contract.Value(&_Smartcontracts.CallOpts)
}

// Abort is a paid mutator transaction binding the contract method 0x35a063b4.
//
// Solidity: function abort() returns()
func (_Smartcontracts *SmartcontractsTransactor) Abort(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "abort")
}

// Abort is a paid mutator transaction binding the contract method 0x35a063b4.
//
// Solidity: function abort() returns()
func (_Smartcontracts *SmartcontractsSession) Abort() (*types.Transaction, error) {
	return _Smartcontracts.Contract.Abort(&_Smartcontracts.TransactOpts)
}

// Abort is a paid mutator transaction binding the contract method 0x35a063b4.
//
// Solidity: function abort() returns()
func (_Smartcontracts *SmartcontractsTransactorSession) Abort() (*types.Transaction, error) {
	return _Smartcontracts.Contract.Abort(&_Smartcontracts.TransactOpts)
}

// ConfirmFreelancer is a paid mutator transaction binding the contract method 0xc7e13a2a.
//
// Solidity: function confirmFreelancer() returns()
func (_Smartcontracts *SmartcontractsTransactor) ConfirmFreelancer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "confirmFreelancer")
}

// ConfirmFreelancer is a paid mutator transaction binding the contract method 0xc7e13a2a.
//
// Solidity: function confirmFreelancer() returns()
func (_Smartcontracts *SmartcontractsSession) ConfirmFreelancer() (*types.Transaction, error) {
	return _Smartcontracts.Contract.ConfirmFreelancer(&_Smartcontracts.TransactOpts)
}

// ConfirmFreelancer is a paid mutator transaction binding the contract method 0xc7e13a2a.
//
// Solidity: function confirmFreelancer() returns()
func (_Smartcontracts *SmartcontractsTransactorSession) ConfirmFreelancer() (*types.Transaction, error) {
	return _Smartcontracts.Contract.ConfirmFreelancer(&_Smartcontracts.TransactOpts)
}

// ConfirmJobDone is a paid mutator transaction binding the contract method 0x18083adc.
//
// Solidity: function confirmJobDone() returns()
func (_Smartcontracts *SmartcontractsTransactor) ConfirmJobDone(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "confirmJobDone")
}

// ConfirmJobDone is a paid mutator transaction binding the contract method 0x18083adc.
//
// Solidity: function confirmJobDone() returns()
func (_Smartcontracts *SmartcontractsSession) ConfirmJobDone() (*types.Transaction, error) {
	return _Smartcontracts.Contract.ConfirmJobDone(&_Smartcontracts.TransactOpts)
}

// ConfirmJobDone is a paid mutator transaction binding the contract method 0x18083adc.
//
// Solidity: function confirmJobDone() returns()
func (_Smartcontracts *SmartcontractsTransactorSession) ConfirmJobDone() (*types.Transaction, error) {
	return _Smartcontracts.Contract.ConfirmJobDone(&_Smartcontracts.TransactOpts)
}

// CreateEscrow is a paid mutator transaction binding the contract method 0x8da2c873.
//
// Solidity: function createEscrow(_tokenID uint256) returns()
func (_Smartcontracts *SmartcontractsTransactor) CreateEscrow(opts *bind.TransactOpts, _tokenID *big.Int) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "createEscrow", _tokenID)
}

// CreateEscrow is a paid mutator transaction binding the contract method 0x8da2c873.
//
// Solidity: function createEscrow(_tokenID uint256) returns()
func (_Smartcontracts *SmartcontractsSession) CreateEscrow(_tokenID *big.Int) (*types.Transaction, error) {
	return _Smartcontracts.Contract.CreateEscrow(&_Smartcontracts.TransactOpts, _tokenID)
}

// CreateEscrow is a paid mutator transaction binding the contract method 0x8da2c873.
//
// Solidity: function createEscrow(_tokenID uint256) returns()
func (_Smartcontracts *SmartcontractsTransactorSession) CreateEscrow(_tokenID *big.Int) (*types.Transaction, error) {
	return _Smartcontracts.Contract.CreateEscrow(&_Smartcontracts.TransactOpts, _tokenID)
}

// DisputeAsCompany is a paid mutator transaction binding the contract method 0xc1521518.
//
// Solidity: function disputeAsCompany() returns()
func (_Smartcontracts *SmartcontractsTransactor) DisputeAsCompany(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "disputeAsCompany")
}

// DisputeAsCompany is a paid mutator transaction binding the contract method 0xc1521518.
//
// Solidity: function disputeAsCompany() returns()
func (_Smartcontracts *SmartcontractsSession) DisputeAsCompany() (*types.Transaction, error) {
	return _Smartcontracts.Contract.DisputeAsCompany(&_Smartcontracts.TransactOpts)
}

// DisputeAsCompany is a paid mutator transaction binding the contract method 0xc1521518.
//
// Solidity: function disputeAsCompany() returns()
func (_Smartcontracts *SmartcontractsTransactorSession) DisputeAsCompany() (*types.Transaction, error) {
	return _Smartcontracts.Contract.DisputeAsCompany(&_Smartcontracts.TransactOpts)
}

// DisputeAsFreelancer is a paid mutator transaction binding the contract method 0x8ab5c874.
//
// Solidity: function disputeAsFreelancer() returns()
func (_Smartcontracts *SmartcontractsTransactor) DisputeAsFreelancer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Smartcontracts.contract.Transact(opts, "disputeAsFreelancer")
}

// DisputeAsFreelancer is a paid mutator transaction binding the contract method 0x8ab5c874.
//
// Solidity: function disputeAsFreelancer() returns()
func (_Smartcontracts *SmartcontractsSession) DisputeAsFreelancer() (*types.Transaction, error) {
	return _Smartcontracts.Contract.DisputeAsFreelancer(&_Smartcontracts.TransactOpts)
}

// DisputeAsFreelancer is a paid mutator transaction binding the contract method 0x8ab5c874.
//
// Solidity: function disputeAsFreelancer() returns()
func (_Smartcontracts *SmartcontractsTransactorSession) DisputeAsFreelancer() (*types.Transaction, error) {
	return _Smartcontracts.Contract.DisputeAsFreelancer(&_Smartcontracts.TransactOpts)
}

// SmartcontractsAbortedIterator is returned from FilterAborted and is used to iterate over the raw logs and unpacked data for Aborted events raised by the Smartcontracts contract.
type SmartcontractsAbortedIterator struct {
	Event *SmartcontractsAborted // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SmartcontractsAbortedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SmartcontractsAborted)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SmartcontractsAborted)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SmartcontractsAbortedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SmartcontractsAbortedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SmartcontractsAborted represents a Aborted event raised by the Smartcontracts contract.
type SmartcontractsAborted struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterAborted is a free log retrieval operation binding the contract event 0x72c874aeff0b183a56e2b79c71b46e1aed4dee5e09862134b8821ba2fddbf8bf.
//
// Solidity: e Aborted()
func (_Smartcontracts *SmartcontractsFilterer) FilterAborted(opts *bind.FilterOpts) (*SmartcontractsAbortedIterator, error) {

	logs, sub, err := _Smartcontracts.contract.FilterLogs(opts, "Aborted")
	if err != nil {
		return nil, err
	}
	return &SmartcontractsAbortedIterator{contract: _Smartcontracts.contract, event: "Aborted", logs: logs, sub: sub}, nil
}

// WatchAborted is a free log subscription operation binding the contract event 0x72c874aeff0b183a56e2b79c71b46e1aed4dee5e09862134b8821ba2fddbf8bf.
//
// Solidity: e Aborted()
func (_Smartcontracts *SmartcontractsFilterer) WatchAborted(opts *bind.WatchOpts, sink chan<- *SmartcontractsAborted) (event.Subscription, error) {

	logs, sub, err := _Smartcontracts.contract.WatchLogs(opts, "Aborted")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SmartcontractsAborted)
				if err := _Smartcontracts.contract.UnpackLog(event, "Aborted", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// SmartcontractsConfirmedFreelancerIterator is returned from FilterConfirmedFreelancer and is used to iterate over the raw logs and unpacked data for ConfirmedFreelancer events raised by the Smartcontracts contract.
type SmartcontractsConfirmedFreelancerIterator struct {
	Event *SmartcontractsConfirmedFreelancer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SmartcontractsConfirmedFreelancerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SmartcontractsConfirmedFreelancer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SmartcontractsConfirmedFreelancer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SmartcontractsConfirmedFreelancerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SmartcontractsConfirmedFreelancerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SmartcontractsConfirmedFreelancer represents a ConfirmedFreelancer event raised by the Smartcontracts contract.
type SmartcontractsConfirmedFreelancer struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterConfirmedFreelancer is a free log retrieval operation binding the contract event 0xcb099c16b5ac439595a04aee5d3d98570ffc37e017512ba4f9ed0fb6da09edc0.
//
// Solidity: e ConfirmedFreelancer()
func (_Smartcontracts *SmartcontractsFilterer) FilterConfirmedFreelancer(opts *bind.FilterOpts) (*SmartcontractsConfirmedFreelancerIterator, error) {

	logs, sub, err := _Smartcontracts.contract.FilterLogs(opts, "ConfirmedFreelancer")
	if err != nil {
		return nil, err
	}
	return &SmartcontractsConfirmedFreelancerIterator{contract: _Smartcontracts.contract, event: "ConfirmedFreelancer", logs: logs, sub: sub}, nil
}

// WatchConfirmedFreelancer is a free log subscription operation binding the contract event 0xcb099c16b5ac439595a04aee5d3d98570ffc37e017512ba4f9ed0fb6da09edc0.
//
// Solidity: e ConfirmedFreelancer()
func (_Smartcontracts *SmartcontractsFilterer) WatchConfirmedFreelancer(opts *bind.WatchOpts, sink chan<- *SmartcontractsConfirmedFreelancer) (event.Subscription, error) {

	logs, sub, err := _Smartcontracts.contract.WatchLogs(opts, "ConfirmedFreelancer")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SmartcontractsConfirmedFreelancer)
				if err := _Smartcontracts.contract.UnpackLog(event, "ConfirmedFreelancer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// SmartcontractsDisputeCompanyIterator is returned from FilterDisputeCompany and is used to iterate over the raw logs and unpacked data for DisputeCompany events raised by the Smartcontracts contract.
type SmartcontractsDisputeCompanyIterator struct {
	Event *SmartcontractsDisputeCompany // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SmartcontractsDisputeCompanyIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SmartcontractsDisputeCompany)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SmartcontractsDisputeCompany)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SmartcontractsDisputeCompanyIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SmartcontractsDisputeCompanyIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SmartcontractsDisputeCompany represents a DisputeCompany event raised by the Smartcontracts contract.
type SmartcontractsDisputeCompany struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterDisputeCompany is a free log retrieval operation binding the contract event 0x13ab9f3771939d079cbbfd3fac6d5eb557018fb491aa068c5ecaae963ab3fdcc.
//
// Solidity: e DisputeCompany()
func (_Smartcontracts *SmartcontractsFilterer) FilterDisputeCompany(opts *bind.FilterOpts) (*SmartcontractsDisputeCompanyIterator, error) {

	logs, sub, err := _Smartcontracts.contract.FilterLogs(opts, "DisputeCompany")
	if err != nil {
		return nil, err
	}
	return &SmartcontractsDisputeCompanyIterator{contract: _Smartcontracts.contract, event: "DisputeCompany", logs: logs, sub: sub}, nil
}

// WatchDisputeCompany is a free log subscription operation binding the contract event 0x13ab9f3771939d079cbbfd3fac6d5eb557018fb491aa068c5ecaae963ab3fdcc.
//
// Solidity: e DisputeCompany()
func (_Smartcontracts *SmartcontractsFilterer) WatchDisputeCompany(opts *bind.WatchOpts, sink chan<- *SmartcontractsDisputeCompany) (event.Subscription, error) {

	logs, sub, err := _Smartcontracts.contract.WatchLogs(opts, "DisputeCompany")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SmartcontractsDisputeCompany)
				if err := _Smartcontracts.contract.UnpackLog(event, "DisputeCompany", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// SmartcontractsDisputeFreelancerIterator is returned from FilterDisputeFreelancer and is used to iterate over the raw logs and unpacked data for DisputeFreelancer events raised by the Smartcontracts contract.
type SmartcontractsDisputeFreelancerIterator struct {
	Event *SmartcontractsDisputeFreelancer // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SmartcontractsDisputeFreelancerIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SmartcontractsDisputeFreelancer)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SmartcontractsDisputeFreelancer)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SmartcontractsDisputeFreelancerIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SmartcontractsDisputeFreelancerIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SmartcontractsDisputeFreelancer represents a DisputeFreelancer event raised by the Smartcontracts contract.
type SmartcontractsDisputeFreelancer struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterDisputeFreelancer is a free log retrieval operation binding the contract event 0x42f622ee008bee30fa709b5e993e0b340494d2ece4b36bfac89bd82b65aadff7.
//
// Solidity: e DisputeFreelancer()
func (_Smartcontracts *SmartcontractsFilterer) FilterDisputeFreelancer(opts *bind.FilterOpts) (*SmartcontractsDisputeFreelancerIterator, error) {

	logs, sub, err := _Smartcontracts.contract.FilterLogs(opts, "DisputeFreelancer")
	if err != nil {
		return nil, err
	}
	return &SmartcontractsDisputeFreelancerIterator{contract: _Smartcontracts.contract, event: "DisputeFreelancer", logs: logs, sub: sub}, nil
}

// WatchDisputeFreelancer is a free log subscription operation binding the contract event 0x42f622ee008bee30fa709b5e993e0b340494d2ece4b36bfac89bd82b65aadff7.
//
// Solidity: e DisputeFreelancer()
func (_Smartcontracts *SmartcontractsFilterer) WatchDisputeFreelancer(opts *bind.WatchOpts, sink chan<- *SmartcontractsDisputeFreelancer) (event.Subscription, error) {

	logs, sub, err := _Smartcontracts.contract.WatchLogs(opts, "DisputeFreelancer")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SmartcontractsDisputeFreelancer)
				if err := _Smartcontracts.contract.UnpackLog(event, "DisputeFreelancer", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// SmartcontractsJobFinishedSuccessfullyIterator is returned from FilterJobFinishedSuccessfully and is used to iterate over the raw logs and unpacked data for JobFinishedSuccessfully events raised by the Smartcontracts contract.
type SmartcontractsJobFinishedSuccessfullyIterator struct {
	Event *SmartcontractsJobFinishedSuccessfully // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *SmartcontractsJobFinishedSuccessfullyIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(SmartcontractsJobFinishedSuccessfully)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(SmartcontractsJobFinishedSuccessfully)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *SmartcontractsJobFinishedSuccessfullyIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *SmartcontractsJobFinishedSuccessfullyIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// SmartcontractsJobFinishedSuccessfully represents a JobFinishedSuccessfully event raised by the Smartcontracts contract.
type SmartcontractsJobFinishedSuccessfully struct {
	Raw types.Log // Blockchain specific contextual infos
}

// FilterJobFinishedSuccessfully is a free log retrieval operation binding the contract event 0xacfc25a9e10ece6a9b3ee7c66ca782e6c75cfcd6171fa0063471e3b038a1ce94.
//
// Solidity: e JobFinishedSuccessfully()
func (_Smartcontracts *SmartcontractsFilterer) FilterJobFinishedSuccessfully(opts *bind.FilterOpts) (*SmartcontractsJobFinishedSuccessfullyIterator, error) {

	logs, sub, err := _Smartcontracts.contract.FilterLogs(opts, "JobFinishedSuccessfully")
	if err != nil {
		return nil, err
	}
	return &SmartcontractsJobFinishedSuccessfullyIterator{contract: _Smartcontracts.contract, event: "JobFinishedSuccessfully", logs: logs, sub: sub}, nil
}

// WatchJobFinishedSuccessfully is a free log subscription operation binding the contract event 0xacfc25a9e10ece6a9b3ee7c66ca782e6c75cfcd6171fa0063471e3b038a1ce94.
//
// Solidity: e JobFinishedSuccessfully()
func (_Smartcontracts *SmartcontractsFilterer) WatchJobFinishedSuccessfully(opts *bind.WatchOpts, sink chan<- *SmartcontractsJobFinishedSuccessfully) (event.Subscription, error) {

	logs, sub, err := _Smartcontracts.contract.WatchLogs(opts, "JobFinishedSuccessfully")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(SmartcontractsJobFinishedSuccessfully)
				if err := _Smartcontracts.contract.UnpackLog(event, "JobFinishedSuccessfully", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}
