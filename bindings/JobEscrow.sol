pragma solidity >=0.4.25 <0.6.0;


contract JobEscrow {

  uint256 public tokenID;
  uint public value;
  address payable public freelancer;
  address payable public company;
  enum State { Created, Ongoing, Inactive, Disputed }
  State public state;

  constructor() public {
    freelancer = msg.sender;
  }

  modifier condition(bool _condition){
    require(_condition, "invalid condition");
    _;
  }

  modifier onlyCompany(){
    require(msg.sender == company, "only the company is allowed to call this");
    _;
  }

  modifier onlyFreelancer(){
    require(msg.sender == freelancer, "only the freelancer is allowed to call this");
    _;
  }

  modifier inState(State _state) {
    require(state == _state, "incorrect job state");
    _;
  }

  event Aborted();
  event ConfirmedFreelancer();
  event JobFinishedSuccessfully();
  event DisputeCompany();
  event DisputeFreelancer();

  // start point for the logic. TokenID is the token from the JobGenerator
  function createEscrow(uint256 _tokenID) public onlyFreelancer payable {
    value = msg.value / 2;
    require((2*value) == msg.value, "freelancer must start it with 2x the value of the job");
    tokenID = _tokenID;
  }

  // Abort can only be called by the freelancer. Ex: if the company doesn't accept it
  function abort() public onlyFreelancer inState(State.Created) {
    emit Aborted();
    state = State.Inactive;
    freelancer.transfer(address(this).balance);
  }

  // Confirms that the company wants to work with this freelancer
  function confirmFreelancer() public inState(State.Created)
   condition(msg.value == (2 * value)) payable {
    emit ConfirmedFreelancer();
    company = msg.sender;
    state = State.Ongoing;
  }

  // Confirm that you (the company) got the job done
  // This is best case scenario where both freelancer & company are ok.
  function confirmJobDone() public onlyCompany inState(State.Ongoing) {
    state = State.Inactive;  // call it first so it doesn't gets called again
    emit JobFinishedSuccessfully();

    company.transfer(value);
    freelancer.transfer(address(this).balance);

  }

  // Call arbitration as a company
  // Job is NOT done, and as a company you want the escrow back
  function disputeAsCompany() public onlyCompany inState(State.Ongoing) {
    state = State.Disputed;  // call it first so it doesn't gets called again
    emit DisputeCompany();

    // logic here
  }

  // Call arbitration as a freelancer
  // Job is DONE, but the company doesn't want to pay
  function disputeAsFreelancer() public onlyFreelancer inState(State.Ongoing) {
    state = State.Disputed;  // call it first so it doesn't gets called again
    emit DisputeFreelancer();

    // logic here
  }
}
